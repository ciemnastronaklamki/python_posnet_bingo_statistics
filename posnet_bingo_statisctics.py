#! python3
#Author: Michał Misztal (ciemna.strona.klamki@gmail.com)

import sys,os,re,codecs

folderGlowny = os.getcwd()

def pobierzPodfoldery(sciezka):
    podfoldery = []
    for v in os.listdir(sciezka):
        if os.path.isdir(os.path.join(sciezka,v)) == True:
            podfoldery.append(os.path.join(sciezka,v))
    return podfoldery

def pobierzListePlikow(listaFolderow):
    pliki = []
    plikiRegEx = re.compile(r'^\d+\.DAY$')
    for v in listaFolderow:
        for w in os.listdir(v):
            if plikiRegEx.search(w):
                pliki.append(os.path.join(v,w))
    pliki.sort()
    return pliki
    
def utworzTabliceSumParagonow(listaPlikow):
    sumy = {"suma":[],"sumaVAT":[]}
    sumyRegEx = re.compile(r'.*SUMA\ PLN\s+(\d+,\d{2}).*')
    sumyVATRegEx = re.compile(r'.*SUMA\ PTU\s+(\d+,\d{2}).*')
    for v in listaPlikow:
        plik = codecs.open(v,"r",encoding="iso-8859-2")
        cont = plik.read()
        mo = sumyRegEx.findall(cont)
        mov = sumyVATRegEx.findall(cont)
        plik.close()
        sumy["suma"] += mo
        sumy["sumaVAT"] += mov
    return sumy
    
def sumuj(tab):    
    sumaP = 0.00
    sumaV = float(0)
        
    for v in tab['suma']:
        sumaP += float(v.replace(',', '.'))
        
    for v in tab['sumaVAT']:
        sumaV += float(v.replace(',', '.'))
        
    print("Suma paragonów wynosi %f\n" % (sumaP))
    print("Suma podatku VAT wynosi %f" % (sumaV))
    
    
listaPodfolderow = pobierzPodfoldery(folderGlowny)
listaPlikow = pobierzListePlikow(listaPodfolderow)
listaSum = utworzTabliceSumParagonow(listaPlikow)

suma = sumuj(listaSum)
